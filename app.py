import falcon
import json

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn scala',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    },
    {
        'id': 3,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    },
    {
        'id': 4,
        'title': u'Learn java',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    },
    {
        'id': 5,
        'title': u'Learn go',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]


class TaskUno(object):
    def on_get(self, req, resp):
        resp.body = json.dumps(tasks)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(tasks)


class TaskDos(object):
    def on_get(self, req, resp, value):
        resp.status = falcon.HTTP_200
        if value == "javier":
            resp.body = json.dumps({"estado": "found"})
        else:
            resp.body = json.dumps({"estado": "not found"})


api = falcon.API()
api.add_route('/tk', TaskUno())
api.add_route('/task/{value}', TaskDos())
